import Ember from 'ember';

export default Ember.Component.extend({
  file: undefined,

  filesSelected: function(event) {
    var files = event.target.files;
    this.get('filesLoaded')(files[0])
  },

  didInsertElement: function() {
    this.$('input').on(
      'change', Ember.run.bind(this, 'filesSelected')
    );
  }
});
