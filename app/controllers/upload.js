import Ember from 'ember';

export default Ember.Controller.extend({
  actions:{
    filesLoaded(file) {
      this.get('model').set('attachment', file);
      //this.get('model').save();

      if (file){
          var reader = new FileReader();

          reader.onload = function (file, conteudo_arquivo) {
            $('#download').attr('href', file.target.result);
            $('#image').attr('src', file.target.result);

            return file.target.result;
          }
          reader.readAsDataURL(file);
      }
    }
  }
});
